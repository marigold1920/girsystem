package gir.application.crawler;

import gir.application.checker.ElementChecker;
import gir.application.constance.WebConstance;
import gir.application.crawler.helper.CrawlingHelper;
import lombok.SneakyThrows;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Collection;

public class AccountDetailsCrawler {

        @SneakyThrows
        public  Collection<String> getAccountImages(String document) {
                Collection<String> imageLinks = new ArrayList<>();
                XMLEventReader eventReader = CrawlingHelper.parseStringToXMLEventReader(document);
                XMLEvent event;

                boolean isFound = false;
                boolean isStart = false;

                boolean isImage = false;

                while (eventReader.hasNext()) {
                        event = eventReader.nextEvent();

                        if (event.isStartElement()) {
                                StartElement startElement = event.asStartElement();
                                if (ElementChecker.isElementWith(startElement, "div", "class", "container m-t-20 content_post")) {
                                        isFound  = true;
                                }

                                if (isFound && ElementChecker.isElementWith(startElement, "img", "class", "zoom")) {
                                        String imageLink = WebConstance.SHOPTIENZOMBIE + startElement.getAttributeByName(new QName("src")).getValue();
                                        imageLinks.add(imageLink);
                                }
                        }
                }

                return imageLinks;
        }
}
