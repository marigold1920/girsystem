package gir.application.crawler;

import gir.application.constance.WebConstance;
import gir.application.crawler.helper.CrawlingHelper;
import gir.application.crawler.helper.ParsingResult;
import gir.application.crawler.shoptienzombie.AccountCrawler;
import gir.application.dao.AccountDAO;
import gir.application.model.Account;
import gir.application.utils.HtmlNormalization;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.Collection;

@WebServlet(name = "CrawlerServlet", urlPatterns = {"/CrawlerServlet"})
public class CrawlerServlet extends HttpServlet {
        @SneakyThrows
        @Override
        protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                String content = "";
                String line = "";
                int count = 0;
                String link = WebConstance.SHOPTIENZOMBIE_MAIN_URL;
                AccountCrawler accountCrawler = new AccountCrawler();
                AccountDetailsCrawler detailsCrawler = new AccountDetailsCrawler();
                ParsingResult<Account> result;

//                while (link != null && !link.trim().isBlank()) {
//                        content = HtmlNormalization.refineHtml(CrawlingHelper.normalizeHTML(link));
//                        result = accountCrawler.parseXMLToAccountModel(content);
//                        Collection<Account> data = result.getData();
//
//                        if (!data.isEmpty()) {
//                                AccountDAO.saveAccounts(result.getData());
//                        }
//
//                        for (Account account : data) {
//                                content = HtmlNormalization.refineHtml(CrawlingHelper.normalizeHTML(account.getLink()));
//                                Collection<String> imageLinks = detailsCrawler.getAccountImages(content);
//                               for (String imageLink : imageLinks) {
//                                        ReadableByteChannel readableByteChannel = Channels.newChannel(new URL(imageLink).openStream());
//                                        FileOutputStream fileOutputStream = new FileOutputStream("/home/victor/Downloads/TrainImage/image" + (++count) + ".jpg");
//                                        FileChannel fileChannel = fileOutputStream.getChannel();
//                                        fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
//                                }
//                        }
//
//                        link = result.getPageNext();
//                }
                content = HtmlNormalization.refineHtml(CrawlingHelper.normalizeHTML(link));
                BufferedReader bf = new BufferedReader(new FileReader("/home/victor/MEGA/PRX301/GIRSystem/src/main/webapp/WEB-INF/accounts.xml"));
                while ((line = bf.readLine()) != null) {
                        content += line;
                }
//                result = accountCrawler.parseXMLToAccountModel(content);
//                result.getData().forEach(System.out::println);
                StreamSource streamSource = new StreamSource(new StringReader(content));
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
//                transformer.tran
                bf.close();
        }
}
