package gir.application.dao;

import gir.application.model.Account;
import gir.application.utils.DBConnection;
import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class AccountDAO {

        @SneakyThrows
        public static void saveAccounts(Collection<Account> accounts) {
                String sql = "INSERT INTO Account (price, num_of_hero, num_of_skins, ranking, link) VALUES(?,?,?,?,?)";

                try (Connection connection = DBConnection.getConnection();
                     PreparedStatement pstm = connection.prepareStatement(sql)) {
                        for (Account account : accounts) {
                                pstm.setDouble(1, account.getPrice());
                                pstm.setInt(2, account.getNumOfHero());
                                pstm.setInt(3, account.getNumOfSkins());
                                pstm.setString(4, account.getRank());
                                pstm.setString(5, account.getLink());
                                pstm.addBatch();
                        }

                        pstm.executeBatch();
                }
        }

        @SneakyThrows
        public static Collection<String> getAccountURL() {
                String sql = "SELECT link FROM Account";
                Collection<String> urls = new ArrayList<>();

                try (Connection connection = DBConnection.getConnection();
                     Statement stmt = connection.createStatement();
                     ResultSet rs = stmt.executeQuery(sql)) {
                        while (rs.next()) {
                                urls.add(rs.getString("link"));
                        }
                }

                return urls;
        }
}
