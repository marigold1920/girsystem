package gir.application.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Account {
        private double price;
        private short numOfHero;
        private short numOfSkins;
        private String rank;
        private String link;

        @Override
        public String toString() {
                return "Account{" +
                        "price=" + price +
                        ", numOfHero=" + numOfHero +
                        ", numOfSkins=" + numOfSkins +
                        ", rank='" + rank + '\'' +
                        ", link='" + link + '\'' +
                        '}';
        }
}
