package gir.application.crawler.shoptienzombie;

import gir.application.checker.ElementChecker;
import gir.application.constance.WebConstance;
import gir.application.crawler.helper.CrawlingHelper;
import gir.application.crawler.helper.ParsingResult;
import gir.application.model.Account;
import lombok.SneakyThrows;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Collection;

public class AccountCrawler {

        @SneakyThrows
        public ParsingResult<Account> parseXMLToAccountModel(String document) {
                ParsingResult<Account> result = new ParsingResult<>();
                Collection<Account> accounts = new ArrayList<>();
                XMLEventReader eventReader = CrawlingHelper.parseStringToXMLEventReader(document);
                XMLEvent event;
                boolean isFound = false;
                boolean isStart = false;

                boolean isAccount = false;

                boolean isPrice = false;
                boolean isNumOfHero = false;
                boolean isNumOfSkins = false;
                boolean isRank = false;
                boolean isLink = false;
                boolean isPageNext = false;

                double price = 0;
                short numOfHero = 0;
                short numOfSkins = 0;
                String rank = "";
                String link = "";

                while (eventReader.hasNext()) {
                        event = eventReader.nextEvent();

                        if (event.isStartElement()) {
                                StartElement startElement = event.asStartElement();
                                if (ElementChecker.isElementWith(startElement, "div", "class", "row row-flex item-list")) {
                                        isFound = true;
                                }

                                if (isStart) {
                                        if (ElementChecker.isElementWith(startElement, "div", "class", "attribute_info")) {
                                                isNumOfHero = true;
                                        }

                                        if (isNumOfHero && ElementChecker.isElementWith(startElement, "b")) {
                                                Characters characters = eventReader.nextEvent().asCharacters();
                                                numOfHero = Short.parseShort(characters.getData().trim());
                                                isNumOfHero = false;
                                                isNumOfSkins = true;
                                                startElement = null;
                                        }

                                        if (isNumOfSkins && startElement != null &&   ElementChecker.isElementWith(startElement, "b")) {
                                                Characters characters = eventReader.nextEvent().asCharacters();
                                                numOfSkins = Short.parseShort(characters.getData().trim());
                                                isNumOfSkins = false;
                                                isRank = true;
                                                startElement = null;
                                        }

                                        if (isRank && startElement != null && ElementChecker.isElementWith(startElement, "b")) {
                                                Characters characters = eventReader.nextEvent().asCharacters();
                                                rank = characters.getData().trim();
                                                isRank = false;
                                                isPrice = true;
                                                startElement = null;
                                        }

                                        if (isPrice && startElement != null && ElementChecker.isElementWith(startElement, "div", "class", "price_item")) {
                                                Characters characters = eventReader.nextEvent().asCharacters();
                                                price = Double.parseDouble(characters.getData().trim().replaceAll("\\D+", ""));
                                                isPrice = false;
                                                isLink = true;
                                        }

                                        if (isLink && ElementChecker.isElementWith(startElement, "a")) {
                                                link = WebConstance.SHOPTIENZOMBIE +  startElement.getAttributeByName(new QName("href")).getValue();
                                                Account account = new Account(price, numOfHero, numOfSkins, rank, link);
                                                accounts.add(account);
                                                isLink = false;
                                                isStart = false;
                                        }
                                }

                                if (isPageNext && ElementChecker.isElementWith(startElement, "a", "rel", "next")) {
                                        String pageNext = startElement.getAttributeByName(new QName("href")).getValue();
                                        result.setPageNext(pageNext);
                                }
                        }

                        if (isFound && event.isStartElement()) {
                                StartElement startElement = event.asStartElement();
                                if (ElementChecker.isElementWith(startElement, "div", "class", "col-sm-6 col-md-3")) {
                                        isStart = true;
                                }
                        }

                        if (!isStart && event.isStartElement()) {
                                StartElement startElement = event.asStartElement();
                                if (ElementChecker.isElementWith(startElement, "ul", "class", "pagination pagination-sm")) {
                                        isPageNext = true;
                                }
                        }
                }

                result.setData(accounts);

                return result;
        }
}
